# fmrplot
An analysis tool for FMR data taken at TRIUMF. Its goal is to provide a convenient way to visualize (and process) the output of `lockin-sweep`.


### Building fmrplot
Requirements:
- `ROOT`
- `c++11` compatable compiler
- `X11` and `Xrandr`

A makefile is provided for easy building:
```bash
$ cd <path/to/fmrplot>
$ make
```

### Running fmrplot
`fmrplot` requires the location of files generated from `lockin-sweep` as command line arguments. A single file is required, but an arbitrary number can be specified. For example:
```bash
$ ./fmrplot <path/to/file.res>
```
or
```bash
$ ./fmrplot <path/to/file1.res> <path/to/file2.res> <path/to/file3.res> 
```
are both valid. Upon valid input, a window will display graphs of all quantities of interest for each input file. An output file for each input (e.g., `file.out`) that contains the phase and phase-corrected lock-in will also be generated. An image of the canvas can be saved using the `ROOT` drop-down menu.
