
#include "FMR.h"

#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>

FMR::FMR(const std::string file) 
{
	Filename = file;
	// Open the file, read each line, and pass data into vectors.
   std::ifstream input_file(Filename);
   if ( input_file.is_open() )
   {
      std::string input_line;
      while ( std::getline(input_file, input_line) )
      {
         double i, h, x, y, n, r, theta;
         std::istringstream input_ss( input_line );
         if ( input_ss >> i >> h >> x >> y >> n )
         {
            //std::cout << i << "\t" << h << "\t" << x << "\t" << y << "\t" << n << std::endl;
            x *= 1e6;
            y *= 1e6;
            r = sqrt( x*x + y*y );
            theta = std::atan2(y, x)*180.0/std::acos(-1.0);
            Current.push_back(i);
            Field.push_back(h);
            X.push_back(x);
            Y.push_back(y);
            N.push_back(n);
            R.push_back(r);
            Theta.push_back(theta);
         }
      }
   }
   else
   {
      std::cout << "WARNING: file `" << Filename << "` not found." << std::endl;
   }
   // Easy access to number of points per data set.
   DataPoints = Field.size();
} // FMR::FMR


void FMR::Write()
{
   // Remove extension from filename
   // http://stackoverflow.com/a/6417880
   std::string filename_raw = Filename.substr(0, Filename.find_last_of("."));
   std::string filename_out( filename_raw + ".out" );
   
   std::ofstream outputfile;
   outputfile.open( filename_out.c_str() );
   outputfile << "#Field(G)\tAmplitude(uV)\tPhase(deg.)\n";
   for (std::size_t i = 0; i < Field.size(); ++i)
   {
       outputfile << std::fixed << std::setprecision(9) <<  Field.at(i) << "\t" << R.at(i) << "\t" << Theta.at(i) << "\n";
   }
   outputfile.close();
} // FMR::Write

