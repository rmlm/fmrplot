
#ifndef AESTHETICS_H
#define AESTHETICS_H

#include <TStyle.h>

#include <X11/Xlib.h>
#include <X11/extensions/Xrandr.h>

// Set asthetics for ROOT
void SetAesthetics()
{
   gStyle->SetPadTickX(1);             // X-Axis Border Ticks
   gStyle->SetPadTickY(1);             // Y-Axis Border Ticks
   gStyle->SetPadGridX(0);             // X-Axis Major Gridlines
   gStyle->SetPadGridY(0);             // Y-Axis Major Gridlines
   gStyle->SetPadTopMargin(0.15);		// 0.1
   gStyle->SetPadBottomMargin(0.15);	// 0.1
   gStyle->SetPadLeftMargin(0.15);		// 0.1
   gStyle->SetPadRightMargin(0.15);	   // 0.1
   gStyle->SetLabelFont(42,"xyz");		// Axes label fonts
   gStyle->SetTitleFont(42,"xyz");		// Axes title fonts
   gStyle->SetTitleOffset(1.0,"xyz");	// Axes title position offset
   gStyle->SetTitleSize(0.06,"xyz"); 	// Axes title size
   gStyle->SetTitleBorderSize(0);		// Border box around Histogram/Graph title
   gStyle->SetTitleFont(42,"1");		   // Histogram/Graph title font - arg in "" must not be x, y, or z
   gStyle->SetTitleSize(0.06,"1"); 	   // Axes title size
   gStyle->SetHistMinimumZero(kTRUE);	// Zooms in on all points
   gStyle->SetStripDecimals(kFALSE);   // Keep Constant Decimals
   gStyle->SetOptStat(0);              // Options: ksiourmen 111111110
   gStyle->SetOptFit(0);               // 
   gStyle->SetOptTitle(1);	            // Display title
   
   // Get the resolution of the current display and use it to determine the size of the canvas.
   // http://superuser.com/a/338697
   /*
   std::string get_width = "xrandr --current | grep '*' | uniq | awk '{print $1}' | cut -d 'x' -f1";
   std::string get_height = "xrandr --current | grep '*' | uniq | awk '{print $1}' | cut -d 'x' -f2";
   double screen_height;
   FILE *pin = popen(get_height.c_str(), "r");
	char buff[2048];
	if ( pin != 0 )
	{
      fgets(buff, sizeof(buff), pin);
      screen_height = std::atof(buff);
   }
   else
   {
      screen_height = 1080;
   }
   pclose(pin);
   
   if ( screen_height <= 0 )
   {
      screen_height = 1080;
   }
   */
   
   // Get the resolution of the current display and use it to determine the size of the canvas.
   // Do this without resorting to popen
   // http://stackoverflow.com/a/22461871
   /*
   Display *dpy;
   XRRScreenResources *screen;
   XRRCrtcInfo *crtc_info;
   dpy = XOpenDisplay(":0");   
   screen = XRRGetScreenResources(dpy, DefaultRootWindow(dpy));
   //0 to get the first monitor 
   crtc_info = XRRGetCrtcInfo(dpy, screen, screen->crtcs[0]);
   //double screen_width = crtc_info->width;
   double screen_height = crtc_info->height;
   */
   
   // http://www.cplusplus.com/forum/unices/108795/
   Display *the_display = XOpenDisplay(nullptr);
   Screen *the_screen = DefaultScreenOfDisplay(the_display);
   double screen_height = the_screen->height;
   double screen_width = the_screen->width;
   // Canvas dimensions (4:3)
   double aspect_ratio = 3.0/2.0;
   double screen_fraction = 0.75;
   double height = screen_height*screen_fraction;
   double width = height*aspect_ratio;
   
   // check if too wide, and rescale if necessary
   while ( width > screen_width )
   {
      double down_scale = screen_width/width;
      height *= down_scale;
      width *= down_scale;
   }
   
   gStyle->SetCanvasDefH(height);      // Canvas height (def. 500)	
   gStyle->SetCanvasDefW(width);       // Canvas width (def. 700)
   gStyle->SetCanvasDefX(0);           // Canvas position on monitor
   gStyle->SetCanvasDefY(0);           // Canvas position on monitor
   //gStyle->SetPaperSize(21.6,27.9);  // Size in cm - US Letter Paper
   gStyle->SetPalette(55);             // Rainbow Palette... change this!
   gStyle->SetColorModelPS(0);         // RGB=0, CMYK=1
   gStyle->SetLineScalePS(3);          // Line scaling for Postscript output (3-5 seems OK)
} 

#endif // AESTHETICS_H
