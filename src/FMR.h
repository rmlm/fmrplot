
#ifndef FMR_H
#define FMR_H

#include <string>
#include <vector>

class FMR
{
public :
   FMR(const std::string file);  // Constructor
   void Write();                 // Write R & Phase to <Filename>.out
   std::string Filename;         // Filename
   int DataPoints;               // Data points in file
   std::vector<double> Current;  // HH Current (A)
   std::vector<double> Field;    // Magnetic Field (G)
   std::vector<double> X;        // X Lock-in (uV)
   std::vector<double> Y;        // Y Lock-in (uV)
   std::vector<double> N;        // Number of point in RMS
   std::vector<double> R;        // Phase Corrected Lock-In (uV)
   std::vector<double> Theta;    // Phase (deg)
};

#endif // FMR_H
