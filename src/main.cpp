
#include "FMR.h"
#include "aesthetics.h"
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>

#include <TApplication.h>
#include <TCanvas.h>
#include <TPaveText.h>
#include <TF1.h>
#include <TGraph.h>
#include <TList.h>
#include <TMultiGraph.h>
#include <TText.h>
#include <TLegend.h>
#include <TColor.h>
#include <TError.h>
#include <TROOT.h>

int main(int argc, char *argv[])
{
   // Check command line arguments
   //for ( int i = 0; i < argc; i++ ) std::cout << i << "\t" << argv[i] << std::endl;
   
   // Accept only 1 (i.e., arc == 2)
   if ( argc == 1 )
   {
      std::cout << "Error: No input file." << std::endl;
      std::cout << "   ./fmrplot <file1.res> <file2.res> ... <fileN.res>" << std::endl;
      return EXIT_FAILURE;
   }
   
   // Collect the command line arguments
   std::vector<std::string> cmd_args;
   for ( int i = 1; i < argc; i++ )
   {
      cmd_args.push_back( std::string(argv[i]) );
      // Check if the file exists (or rather is readable)
      // http://stackoverflow.com/a/12774387
      std::ifstream input_file( cmd_args.at(i-1).c_str() );
      if ( !input_file.good() )
      {
         std::cout << "Can't open file: " << cmd_args.at(i-1) << std::endl;
         return EXIT_FAILURE;
      }
   }
   
   // Create the vectors containing the fmr data
   std::vector<FMR> v_fmr_data;
   for (auto &file_name : cmd_args) v_fmr_data.push_back( FMR(file_name.c_str()) );
   
   // Sanity check:
   //for (auto &v : v_fmr_data) std::cout << "\n" << v.Filename << "\n" << std::endl;
   
   // Write the processed data to .out files
   for (auto &data : v_fmr_data) data.Write();
   
   // Create the vectors containing the graphs
   std::vector<TGraph> v_graph_x;
   std::vector<TGraph> v_graph_y;
   std::vector<TGraph> v_graph_r;
   std::vector<TGraph> v_graph_p;
   std::vector<TGraph> v_graph_n;
   
   for (auto &data : v_fmr_data)
   {
      v_graph_r.push_back( TGraph(data.DataPoints, &data.Field[0], &data.R[0]) );
      v_graph_x.push_back( TGraph(data.DataPoints, &data.Field[0], &data.X[0]) );
      v_graph_y.push_back( TGraph(data.DataPoints, &data.Field[0], &data.Y[0]) );
      v_graph_p.push_back( TGraph(data.DataPoints, &data.Field[0], &data.Theta[0]) );
      v_graph_n.push_back( TGraph(data.DataPoints, &data.Field[0], &data.N[0]) );
   }
   
   
   /*
   // Not needed?
   std::vector<std::string> v_graph_name_r;
   std::vector<std::string> v_graph_name_x;
   std::vector<std::string> v_graph_name_y;
   std::vector<std::string> v_graph_name_p;
   std::vector<std::string> v_graph_name_n;
   
   for (std::size_t i = 0; i < v_fmr_data.size(); ++i)
   {
      // Names for the graphs
      v_graph_name_r.push_back( std::string(cmd_args.at(i) + "-Graph_R") );
      v_graph_name_x.push_back( std::string(cmd_args.at(i) + "-Graph_X") );
      v_graph_name_y.push_back( std::string(cmd_args.at(i) + "-Graph_Y") );
      v_graph_name_p.push_back( std::string(cmd_args.at(i) + "-Graph_P") );
      v_graph_name_n.push_back( std::string(cmd_args.at(i) + "-Graph_N") );
   }
   */
   
   // Set the graph aesthetics
   for(std::size_t i = 0; i < v_fmr_data.size(); ++i)
   {
      // Colours in the range of [0,1]
      //float green = 0.0;
      //float red = 1.0 - static_cast<float>(i)/(static_cast<float>(v_fmr_data.size())-1.0);
      //float blue = static_cast<float>(i)/(static_cast<float>(v_fmr_data.size())-1.0);
      
      // Automatic rainbow palette generation, inspired by:
      // http://krazydad.com/tutorials/makecolors.php
      // Generate RGB colours in the range of [0,1].
      float pi   = std::acos(-1);
      float freq = 2*pi/v_fmr_data.size();
      float mid  = 0.5;
      float amp  = 0.5;
      float red   = amp*std::cos(freq*i + pi*0/3) + mid;
      float blue  = amp*std::cos(freq*i + pi*2/3) + mid;
      float green = amp*std::cos(freq*i + pi*4/3) + mid;
      
      v_graph_r.at(i).SetMarkerStyle(kFullCircle);
      v_graph_r.at(i).SetMarkerColor( TColor::GetColor(red, green, blue) );
      v_graph_r.at(i).SetLineColor( TColor::GetColor(red, green, blue) );
      
      v_graph_x.at(i).SetMarkerStyle(kFullCircle);
      v_graph_x.at(i).SetMarkerColor( TColor::GetColor(red, green, blue) );
      v_graph_x.at(i).SetLineColor( TColor::GetColor(red, green, blue) );
      
      v_graph_y.at(i).SetMarkerStyle(kFullCircle);
      v_graph_y.at(i).SetMarkerColor( TColor::GetColor(red, green, blue) );
      v_graph_y.at(i).SetLineColor( TColor::GetColor(red, green, blue) );
      
      v_graph_p.at(i).SetMarkerStyle(kFullCircle);
      v_graph_p.at(i).SetMarkerColor( TColor::GetColor(red, green, blue) );
      v_graph_p.at(i).SetLineColor( TColor::GetColor(red, green, blue) );
      
      v_graph_n.at(i).SetMarkerStyle(kFullCircle);
      v_graph_n.at(i).SetMarkerColor( TColor::GetColor(red, green, blue) );
      v_graph_n.at(i).SetLineColor( TColor::GetColor(red, green, blue) );
   }
   
   // Make TMultiGraphs for plotting all the vectors of TGraphs
   TMultiGraph mg_r("mg_r",";Field (G);Phase Corrected Lock-In (#muV)");
   TMultiGraph mg_x("mg_x",";Field (G);X Lock-In (#muV)");
   TMultiGraph mg_y("mg_y",";Field (G);Y Lock-In (#muV)");
   TMultiGraph mg_p("mg_p",";Field (G);Phase (deg.)");
   TMultiGraph mg_n("mg_n",";Field (G);Points in RMS");
   
   for(std::size_t i = 0; i < v_graph_r.size(); ++i)
   {
      mg_r.Add( &v_graph_r.at(i) );
      mg_x.Add( &v_graph_x.at(i) );
      mg_y.Add( &v_graph_y.at(i) );
      mg_p.Add( &v_graph_p.at(i) );
      mg_n.Add( &v_graph_n.at(i) );
   }
   
   // Create the ROOT app and set the aesthetics.
   TApplication theApp("theApp", &argc, argv);
   SetAesthetics();
   
   // Create the canvas, divide it, and draw the legend & multigraphs.
   TCanvas canvas("canvas","fmrplot: An analysis tool for FMR data at TRIUMF");
   canvas.Divide(3,2);
   
   canvas.cd(1);
   TLegend data_legend(0.01, 0.01, 0.99, 0.99);
   //data_legend.SetHeader("Files:");
   data_legend.SetFillColor(kWhite);
   data_legend.SetFillColor(kWhite);
   data_legend.SetLineColor(kWhite);
   data_legend.SetBorderSize(0);
   data_legend.SetMargin(0.10);
   //data_legend.AddEntry(static_cast<TObject*>(0), "Files :", "");
   //data_legend.SetTextAlign(12);
   for(std::size_t i = 0; i < v_graph_r.size(); ++i)
   {
      //data_legend.AddEntry(&v_graph_r.at(i), cmd_args.at(i).c_str(), "p");
      if ( cmd_args.at(i).length() > 50 )
      {
         std::string split_filename("#splitline{" + cmd_args.at(i).substr(0,50) + "}{" + cmd_args.at(i).substr(51,cmd_args.at(i).length()) + "}");
         data_legend.AddEntry(&v_graph_r.at(i), split_filename.c_str(), "p");
      }
      else
      {
         data_legend.AddEntry(&v_graph_r.at(i), cmd_args.at(i).c_str(), "p");
      }
      // use #splitline{}{} to better display long filenames?
      // https://root.cern.ch/root/roottalk/roottalk10/0617.html
   }
   data_legend.Draw();
   
   canvas.cd(2);
   canvas.cd(2)->SetFillColor(kWhite);
   //canvas.cd(2)->SetFrameFillColor(kYellow-10);
   canvas.cd(2)->SetFrameFillColor(TColor::GetColor(220,220,220));
   mg_x.Draw("AP");
   
   canvas.cd(3);
   canvas.cd(3)->SetFillColor(kWhite);
   //canvas.cd(3)->SetFrameFillColor(kYellow-10);
   canvas.cd(3)->SetFrameFillColor(TColor::GetColor(220,220,220));
   mg_p.Draw("AP");
   
   canvas.cd(4);
   canvas.cd(4)->SetFillColor(kWhite);
   //canvas.cd(4)->SetFrameFillColor(kYellow-10);
   canvas.cd(4)->SetFrameFillColor(TColor::GetColor(220,220,220));
   mg_n.Draw("AP");
   
   canvas.cd(5);
   canvas.cd(5)->SetFillColor(kWhite);
   //canvas.cd(5)->SetFrameFillColor(kYellow-10);
   canvas.cd(5)->SetFrameFillColor(TColor::GetColor(220,220,220));
   mg_y.Draw("AP");
   
   canvas.cd(6);
   canvas.cd(6)->SetFillColor(kWhite);
   //canvas.cd(6)->SetFrameFillColor(kYellow-10);
   canvas.cd(6)->SetFrameFillColor(TColor::GetColor(220,220,220));
   mg_r.Draw("AP");
   
   // Remove extension from filename
   // http://stackoverflow.com/a/6417880
   /*
   std::string fmr_file_raw = fmr_file.substr(0, fmr_file.find_last_of("."));
   std::string fmr_file_pdf = fmr_file_raw + ".pdf";
   gErrorIgnoreLevel = 3000;
   canvas.SaveAs( fmr_file_pdf.c_str() );
   */
   
   // This terminates the running TApplication when ANY canvas window is closed
   // (i.e., exited with "X") - call it before running the TApplication.
   // https://root.cern.ch/phpBB3/viewtopic.php?t=7183
   TQObject::Connect("TCanvas", "Closed()", "TApplication", gApplication, "Terminate()");
   theApp.Run(kTRUE);
   
   return EXIT_SUCCESS;
}
