TARGET     = fmrplot
INSTALLDIR = 
CXX        = g++
CXXFLAGS   = -O3 -Wall -std=c++11 -fdiagnostics-color=always `root-config --cflags`
LDFLAGS    = 
LDLIBS     = `root-config --glibs` -lX11 -lXrandr
RM         = rm -f
SOURCES    = $(shell find . -name "*.cpp")
OBJECTS    = $(patsubst %.cpp, %.o, $(SOURCES))

all: $(TARGET)

$(TARGET): $(OBJECTS)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $(TARGET) $(OBJECTS) $(LDLIBS) 

depend: .depend

.depend: $(SOURCES)
	$(RM) *~ .depend
	$(CXX) $(CXXFLAGS) -MM $^>>./.depend;

install:
	mkdir -p $(INSTALLDIR)
	mv -p $(TARGET) $(INSTALLDIR)

clean:
	$(RM) $(OBJECTS)
	
cleaner: clean
	$(RM) *~ .depend

cleanest: cleaner
	$(RM) $(TARGET)

include .depend
